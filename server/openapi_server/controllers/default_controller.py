import connexion
import six

from openapi_server.models.issue import Issue  # noqa: E501
from openapi_server import util
from openapi_server import db

def issues_get():  # noqa: E501
    """issues_get

    get the list of issues # noqa: E501


    :rtype: List[Issue]
    """

    issues = Issue.query.all()
    return issues, 200


def issues_number_delete(number):  # noqa: E501
    """issues_number_delete

     # noqa: E501

    :param number:
    :type number: int

    :rtype: None
    """
    Issue.query.filter_by(number=number).delete()
    db.session.commit()
    return 'Successfully deleted', 200


def issues_number_get(number):  # noqa: E501
    """issues_number_get

     # noqa: E501

    :param number:
    :type number: int

    :rtype: Issue
    """
    issue = Issue.query.filter_by(number=number).first()
    if issue is None:
        return "Nothing found", 204
    return issue


def issues_number_put(number):  # noqa: E501
    """issues_number_put

     # noqa: E501

    :param number:
    :type number: int
    :param issue: the issue
    :type issue: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        issue = Issue.from_dict(connexion.request.get_json())  # noqa: E501
        registered_issue = Issue.query.filter_by(number=number).first()
        registered_issue.text = issue.text
        registered_issue.title = issue.title
        db.session.commit()
    return 'Successfully modified !', 200


def issues_post(issue):  # noqa: E501
    """issues_post

    Add a new issue # noqa: E501

    :param issue: the issue
    :type issue: dict | bytes

    :rtype: None
    """
    if connexion.request.is_json:
        issue = Issue.from_dict(connexion.request.get_json())  # noqa: E501
        db.session.add(issue)
        db.session.commit()
    return "issue successfully created", 201
