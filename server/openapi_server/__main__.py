#!/usr/bin/env python3

import os

from openapi_server import app
from openapi_server import db
from openapi_server.models.issue import Issue

from flask_cors import CORS

def init_db():
    """Initializes the db for development purposes
    """
    db.drop_all()
    db.create_all()
    db.session.commit()
    db.session.add(Issue(title="title", text="text"))
    db.session.add(Issue(title="another title", text="another tex"))
    db.session.commit()

if __name__ == '__main__':
    if os.environ['INIT_DB'].upper() == "YES":
        init_db()
    CORS(app.app)
    app.run(port=os.environ['PORT'])

