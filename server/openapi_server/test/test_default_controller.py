# coding: utf-8

from __future__ import absolute_import
import unittest

from flask import json
from six import BytesIO

from openapi_server.models.issue import Issue  # noqa: E501
from openapi_server.test import BaseTestCase


class TestDefaultController(BaseTestCase):
    """DefaultController integration test stubs"""

    def test_issues_get(self):
        """Test case for issues_get

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/issues',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_issues_number_delete(self):
        """Test case for issues_number_delete

        
        """
        headers = { 
        }
        response = self.client.open(
            '/issues/{number}'.format(number=56),
            method='DELETE',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_issues_number_get(self):
        """Test case for issues_number_get

        
        """
        headers = { 
            'Accept': 'application/json',
        }
        response = self.client.open(
            '/issues/{number}'.format(number=56),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_issues_number_put(self):
        """Test case for issues_number_put

        
        """
        query_string = [('issue', {})]
        headers = { 
        }
        response = self.client.open(
            '/issues/{number}'.format(number=56),
            method='PUT',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_issues_post(self):
        """Test case for issues_post

        
        """
        issue = {}
        headers = { 
            'Content-Type': 'application/json',
        }
        response = self.client.open(
            '/issues',
            method='POST',
            headers=headers,
            data=json.dumps(issue),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    unittest.main()
