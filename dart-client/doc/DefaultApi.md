# openapi.api.DefaultApi

## Load the API package
```dart
import 'package:openapi/api.dart';
```

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**issuesGet**](DefaultApi.md#issuesGet) | **GET** /issues | 
[**issuesNumberDelete**](DefaultApi.md#issuesNumberDelete) | **DELETE** /issues/{number} | 
[**issuesNumberGet**](DefaultApi.md#issuesNumberGet) | **GET** /issues/{number} | 
[**issuesNumberPut**](DefaultApi.md#issuesNumberPut) | **PUT** /issues/{number} | 
[**issuesPost**](DefaultApi.md#issuesPost) | **POST** /issues | 


# **issuesGet**
> List<Issue> issuesGet()



get the list of issues

### Example 
```dart
import 'package:openapi/api.dart';

final api_instance = DefaultApi();

try { 
    final result = api_instance.issuesGet();
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->issuesGet: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**List<Issue>**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **issuesNumberDelete**
> issuesNumberDelete(number)



### Example 
```dart
import 'package:openapi/api.dart';

final api_instance = DefaultApi();
final number = 56; // int | 

try { 
    api_instance.issuesNumberDelete(number);
} catch (e) {
    print('Exception when calling DefaultApi->issuesNumberDelete: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **int**|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **issuesNumberGet**
> Issue issuesNumberGet(number)



### Example 
```dart
import 'package:openapi/api.dart';

final api_instance = DefaultApi();
final number = 56; // int | 

try { 
    final result = api_instance.issuesNumberGet(number);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->issuesNumberGet: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **int**|  | 

### Return type

[**Issue**](Issue.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **issuesNumberPut**
> issuesNumberPut(number, issue)



### Example 
```dart
import 'package:openapi/api.dart';

final api_instance = DefaultApi();
final number = 56; // int | 
final issue = ; // Issue | 

try { 
    api_instance.issuesNumberPut(number, issue);
} catch (e) {
    print('Exception when calling DefaultApi->issuesNumberPut: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **int**|  | 
 **issue** | [**Issue**](.md)|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **issuesPost**
> issuesPost(issue)



Add a new issue

### Example 
```dart
import 'package:openapi/api.dart';

final api_instance = DefaultApi();
final issue = Issue(); // Issue | the issue

try { 
    api_instance.issuesPost(issue);
} catch (e) {
    print('Exception when calling DefaultApi->issuesPost: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **issue** | [**Issue**](Issue.md)| the issue | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

