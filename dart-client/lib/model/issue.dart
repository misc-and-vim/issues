//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Issue {
  /// Returns a new [Issue] instance.
  Issue({
    this.title,
    this.text,
    this.number,
  });

  
  String title;

  
  String text;

  
  int number;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Issue &&
     other.title == title &&
     other.text == text &&
     other.number == number;

  @override
  int get hashCode =>
    (title == null ? 0 : title.hashCode) +
    (text == null ? 0 : text.hashCode) +
    (number == null ? 0 : number.hashCode);

  @override
  String toString() => 'Issue[title=$title, text=$text, number=$number]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (title != null) {
      json['title'] = title;
    }
    if (text != null) {
      json['text'] = text;
    }
    if (number != null) {
      json['number'] = number;
    }
    return json;
  }

  /// Returns a new [Issue] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Issue fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Issue(
        title: json['title'],
        text: json['text'],
        number: json['number'],
    );

  static List<Issue> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Issue>[]
      : json.map((v) => Issue.fromJson(v)).toList(growable: true == growable);

  static Map<String, Issue> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Issue>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Issue.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Issue-objects as value to a dart map
  static Map<String, List<Issue>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Issue>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Issue.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }
}

