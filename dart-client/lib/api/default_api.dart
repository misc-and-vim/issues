//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class DefaultApi {
  DefaultApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// get the list of issues
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> issuesGetWithHttpInfo() async {
    final path = '/issues'.replaceAll('{format}', 'json');

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// get the list of issues
  Future<List<Issue>> issuesGet() async {
    final response = await issuesGetWithHttpInfo();
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return (apiClient.deserialize(_decodeBodyBytes(response), 'List<Issue>') as List)
        .map((item) => item as Issue)
        .toList(growable: false);
    }
    return null;
  }

  /// Performs an HTTP 'DELETE /issues/{number}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [int] number (required):
  Future issuesNumberDeleteWithHttpInfo(int number) async {
    // Verify required params are set.
    if (number == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: number');
    }

    final path = '/issues/{number}'.replaceAll('{format}', 'json')
      .replaceAll('{' + 'number' + '}', number.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'DELETE',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [int] number (required):
  Future issuesNumberDelete(int number) async {
    final response = await issuesNumberDeleteWithHttpInfo(number);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
  }

  /// Performs an HTTP 'GET /issues/{number}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [int] number (required):
  Future<Response> issuesNumberGetWithHttpInfo(int number) async {
    // Verify required params are set.
    if (number == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: number');
    }

    final path = '/issues/{number}'.replaceAll('{format}', 'json')
      .replaceAll('{' + 'number' + '}', number.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [int] number (required):
  Future<Issue> issuesNumberGet(int number) async {
    final response = await issuesNumberGetWithHttpInfo(number);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.body != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(_decodeBodyBytes(response), 'Issue') as Issue;
    }
    return null;
  }

  /// Performs an HTTP 'PUT /issues/{number}' operation and returns the [Response].
  /// Parameters:
  ///
  /// * [int] number (required):
  ///
  /// * [Issue] issue (required):
  Future issuesNumberPutWithHttpInfo(int number, Issue issue) async {
    // Verify required params are set.
    if (number == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: number');
    }
    if (issue == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: issue');
    }

    final path = '/issues/{number}'.replaceAll('{format}', 'json')
      .replaceAll('{' + 'number' + '}', number.toString());

    Object postBody;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'issue', issue));

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'PUT',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Parameters:
  ///
  /// * [int] number (required):
  ///
  /// * [Issue] issue (required):
  Future issuesNumberPut(int number, Issue issue) async {
    final response = await issuesNumberPutWithHttpInfo(number, issue);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
  }

  /// Add a new issue
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [Issue] issue (required):
  ///   the issue
  Future issuesPostWithHttpInfo(Issue issue) async {
    // Verify required params are set.
    if (issue == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: issue');
    }

    final path = '/issues'.replaceAll('{format}', 'json');

    Object postBody = issue;

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['application/json'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Add a new issue
  ///
  /// Parameters:
  ///
  /// * [Issue] issue (required):
  ///   the issue
  Future issuesPost(Issue issue) async {
    final response = await issuesPostWithHttpInfo(issue);
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
  }
}
